﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projekt
{

    /// <summary>
    /// klasa przechowujaca imiona i ich ocene (uzywana w ocenie epoki)
    /// </summary>
    class WordGrade : IComparable
    {
        public double grade;
        public string name;

        public WordGrade()
        {
            this.grade = 0;
        }
        public WordGrade(string name, double grade)
        {
            this.name = name;
            this.grade = grade;
        }

        public int CompareTo(object obj)
        {
            if (obj is WordGrade)
            {
                if (this.grade > (obj as WordGrade).grade) return -1;
                if (this.grade < (obj as WordGrade).grade) return 1;
                else return 0;

                //   return this.grade.CompareTo((obj as WordGrade).grade);  // compare user names
            }
            throw new ArgumentException("Object is not a WordGrade");
        }

    }
}
