﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace projekt
{
    /// <summary>
    /// klasa przechowujaca wszystkie sylaby 
    /// </summary>
    [Serializable]
    class Syllabus
    {
        public List<string> syllable;


        public Syllabus()
        {
            syllable = new List<string>();
        }


        public static bool dataExistsAndCurrent(string path)
        {
            if (File.Exists(path))
            {
                DateTime creation = File.GetCreationTime(path);
                DateTime now = DateTime.Now;
                if ((now - creation).TotalDays > 10)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else return false;
        }

        public void Serialize(string path)
        {
            Stream stream = File.Open(path, FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            try
            {
                binaryFormatter.Serialize(stream, this);
            }
            catch (System.Runtime.Serialization.SerializationException e)
            {
                Console.WriteLine("Serialization exception");
            }
            stream.Close();
        }

        public static Syllabus Deserialize(string path)
        {
            Stream stream = File.Open(path, FileMode.Open);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            return (Syllabus)binaryFormatter.Deserialize(stream);
        }
    }
}
