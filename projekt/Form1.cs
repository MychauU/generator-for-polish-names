﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHunspell;
using System.Runtime.InteropServices;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;

namespace projekt
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// silnik spellengine nhunspell oparty na  silniku openoffice dictionary and syllabus
        /// </summary>
        SpellEngine engine;
        /// <summary>
        /// glowna klasa zawierajaca w sobie algorytm genetyczny 
        /// </summary>
        Fitness fitness;
        public delegate void textBoxEditor(List<Label> changableUI, ConcurrentQueue<string> text);
        List<string> names = new List<string>();
        List<Label> Labels;
        ConcurrentQueue<string> ConcurrentNames = new ConcurrentQueue<string>();
        List<CheckBox> checkBoxes;

        public Form1()
        {
            InitializeComponent();
            this.Visible = false;
            Labels = new List<Label>(new Label[] { label1, label2, label3, label4, label5 });
            checkBoxes = new List<CheckBox>(new[] { checkBox1, checkBox2, checkBox3, checkBox4, checkBox5 });
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            AllocConsole();
            // Important: Due to the fact Hunspell will use unmanaged memory you have to serve the IDisposable pattern
            // In this block of code this is be done by a using block. But you can also call hunspell.Dispose()


            // Using the spell engine for server applications
            //silnik jezyka tezarus, synonimy, poprawnosc jezykowa, dzielnie na sylaby
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("SpellEngine - Spell Check/Hyphenation/Thesaurus Engine");
            Console.WriteLine("¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
            Console.WriteLine("High performance spell checking for servers and web servers");
            Console.WriteLine("All functions are tread safe. Implementaion uses multi core/multi processor");
            Console.WriteLine("Multiple Languages can be added via AddLanguage()");
            engine = new SpellEngine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Adding a Language with all dictionaries for Hunspell, Hypen and MyThes");
            Console.WriteLine("¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
            LanguageConfig enConfig = new LanguageConfig();
            enConfig.LanguageCode = "pl";
            enConfig.HunspellAffFile = "pl_PL.aff";
            enConfig.HunspellDictFile = "pl_PL.dic";
            enConfig.HunspellKey = "";
            enConfig.HyphenDictFile = "hyph_pl_PL.dic";
            enConfig.MyThesDatFile = "th_pl_PL_v2.dat";
            Console.WriteLine("Configuration will use " + engine.Processors.ToString() + " processors to serve concurrent requests");
            engine.AddLanguage(enConfig);

            //funkcje ktory powinny wykonac sie tylko raz
            //wczytuje wszydtkie sylaby poprawne polskie zgodnie z enginem do pliku sylabypl.txt
            if (!ClassForStaticFunc.fileExists("sylaby.txt"))
                ClassForStaticFunc.loadAllSyllabes(engine, "pl_PL.dic", "iso-8859-2", "sylaby.txt");
            if (!ClassForStaticFunc.fileExists("sylabyPL.txt"))
                ClassForStaticFunc.loadAllSyllabesRedefinied(engine, "sylaby.txt", "sylabyPL.txt");




            //tutaj pobranie imion z bazy np 100 zenskich i 100 meskich w celu ich oceny (tzn ocena jako zrodla )
            fitness = new Fitness(engine, "imiona.csv", "sylabyPL.txt");
            
            this.Visible = true;


            /*
            Console.WriteLine();
            Console.WriteLine("Check if the word 'Recommendation' is spelled correct");
            bool correct = engine["pl"].Spell("Recommendation");
            Console.WriteLine("Recommendation is spelled " + (correct ? "correct" : "not correct"));

            

            Console.WriteLine();
            Console.WriteLine("Make suggestions for the word 'Recommendatio'");
            List<string> suggestions = engine["pl"].Suggest("Recommendatio");
            Console.WriteLine("There are " + suggestions.Count.ToString() + " suggestions");
            foreach (string suggestion in suggestions)
            {
                Console.WriteLine("Suggestion is: " + suggestion);
            }

            Console.WriteLine("");
            Console.WriteLine("Analyze the word 'decompressed'");
            List<string> morphs = engine["pl"].Analyze("decompressed");
            foreach (string morph in morphs)
            {
                Console.WriteLine("Morph is: " + morph);
            }

            Console.WriteLine("");
            Console.WriteLine("Find the word stem of the word 'decompressed'");
            List<string> stems = engine["pl"].Stem("decompressed");
            foreach (string stem in stems)
            {
                Console.WriteLine("Word Stem is: " + stem);
            }

            Console.WriteLine();
            Console.WriteLine("Generate the plural of 'girl' by providing sample 'boys'");
            List<string> generated = engine["pl"].Generate("girl", "boys");
            foreach (string stem in generated)
            {
                Console.WriteLine("Generated word is: " + stem);
            }

            Console.WriteLine();
            Console.WriteLine("Get the hyphenation of the word 'Recommendation'");
            HyphenResult hyphenated = engine["pl"].Hyphenate("Recommendation");
            Console.WriteLine("'Recommendation' is hyphenated as: " + hyphenated.HyphenatedWord);


            Console.WriteLine("Get the synonyms of the plural word 'cars'");
            Console.WriteLine("hunspell must be used to get the word stem 'car' via Stem().");
            Console.WriteLine("hunspell generates the plural forms of the synonyms via Generate()");
            ThesResult tr = engine["pl"].LookupSynonyms("cars", true);
            if (tr != null)
            {
                if (tr.IsGenerated)
                    Console.WriteLine("Generated over stem (The original word form wasn't in the thesaurus)");
                foreach (ThesMeaning meaning in tr.Meanings)
                {
                    Console.WriteLine();
                    Console.WriteLine("  Meaning: " + meaning.Description);

                    foreach (string synonym in meaning.Synonyms)
                    {
                        Console.WriteLine("    Synonym: " + synonym);

                    }
                }
            }


    */
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();

        private void button1_Click(object sender, EventArgs e)
        {
            textBoxEditor myDel = delegate (List<Label> l, ConcurrentQueue<string> s)
            {
                int count = s.Count;
                for (int i = 0; i < count; i++)
                    l.ElementAt(i).Text = s.ElementAt(i);
            };
            Task.Run(() =>
            {
                string output;
                if (radioButton1.Checked)
                    output = fitness.createManName();
                else
                    output = fitness.createWomanName();
                ConcurrentNames.Enqueue(output);
                while (ConcurrentNames.Count > 5)
                {
                    string k;
                    ConcurrentNames.TryDequeue(out k);
                }
                Invoke(myDel, Labels, ConcurrentNames);
            });
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int count = checkBoxes.Count;
            using (StreamWriter sw =
                new StreamWriter(File.Open("imiona.csv", FileMode.Append), Encoding.UTF8))
            {
                for (int i = 0; i < count; i++)
                {
                    string name = Labels.ElementAt(i).Text;
                    if (checkBoxes.ElementAt(i).Checked && name != string.Empty)
                    {
                        char sex = name.Last() == 'a' ? 'K' : 'M';
                        HyphenResult hyphenated;
                        string[] result;
                        char[] charSeparators = new char[] { '=' };
                        hyphenated = engine["pl"].Hyphenate(name);
                        result = hyphenated.HyphenatedWord.Split(charSeparators,
                            StringSplitOptions.RemoveEmptyEntries);
                        int syllab = result.Length;
                        sw.Write(string.Format("{0};{1};{2}\n", name, sex, syllab));
                    }
                }
            }
        }
    }
}
