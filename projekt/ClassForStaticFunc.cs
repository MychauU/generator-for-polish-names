﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHunspell;
using System.Text.RegularExpressions;
using System.IO;

namespace projekt
{
    class ClassForStaticFunc
    {
        /// <summary>
        /// funkcja wyznaczajaca sylaby polskie z dic z pliku tekstowego, przetwarzajaca zgodnie z z przyjetym jezykiem,
        /// wczytuje  z encodinginem  do utf8 ,zapisujaca  go do pliku
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="filename"></param>
        /// /// <param name="encoding"></param>
        /// /// <param name="destFile"></param>
        public static void loadAllSyllabes(SpellEngine engine, string filename, string encoding, string destFilename)
        {
            string[] lines = File.ReadAllLines(filename, Encoding.GetEncoding(encoding));
            HashSet<string> sylaby = new HashSet<string>();
            char[] charSeparators = new char[] { '=' };
            string[] result;
            string pr;
            Regex regex = new Regex(@"^([a-zA-Zśćżźęąółń]+)$");
            HyphenResult hyphenated;
            foreach (string pom in lines)
            {
                pr = Encoding.GetEncoding(encoding).GetString(Encoding.Convert(Encoding.GetEncoding(encoding), Encoding.UTF8, Encoding.GetEncoding(encoding).GetBytes(pom)));
                int gh = pom.IndexOf('/');
                if (gh >= 0)
                {
                    pr = pom.Remove(gh, pom.Length - gh);
                }
                else
                {
                    pr = pom;
                }
                hyphenated = engine["pl"].Hyphenate(pr);
                result = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
                if (result != null)
                {
                    string poms;
                    foreach (string syl in result)
                    {
                        if (regex.IsMatch(syl))
                        {
                            poms = syl.ToLower();
                            sylaby.Add(poms);
                        }

                    }
                }
            }
            using (StreamWriter sw = new StreamWriter(File.Open(destFilename, FileMode.Create), Encoding.UTF8))
            {
                foreach (string pom in sylaby)
                    sw.WriteLine(pom);
            }
        }


        /// <summary>
        ///  funkcja poprawiajaca sylaby z  pierwszej wersji funckji loadAllSyllabes 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        public static void loadAllSyllabesRedefinied(SpellEngine engine, string v1, string v2)
        {
            string lineRead = null;
            string[] lines = File.ReadAllLines(v1, Encoding.UTF8);
            HashSet<string> sylaby = new HashSet<string>();
            char[] charSeparators = new char[] { '=' };
            string[] result;
            Regex regex = new Regex(@"^[A-Za-zśćżźęąółń]*((i|y|e|a|o|u|ą|ę|ł|ń|ś|ż|ź|ć|ó)+)[A-Za-zśćżźęąółń]*$");
            HyphenResult hyphenated;
            foreach (string pom in lines)
            {
                hyphenated = engine["pl"].Hyphenate(pom);
                if (regex.IsMatch(hyphenated.HyphenatedWord))
                {
                    result = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
                    if (result != null)
                    {
                        string poms;
                        foreach (string syl in result)
                        {
                            poms = syl.ToLower();
                            if (!sylaby.Contains(poms))
                                sylaby.Add(poms);

                        }
                    }
                }
            }
            using (StreamWriter sw = new StreamWriter(File.Open(v2, FileMode.Create), Encoding.UTF8))
            {
                foreach (string pom in sylaby)
                {
                    sw.WriteLine(pom);
                }
            }
        }

        public static bool fileExists(string path)
        {
            if (File.Exists(path))
            {
                return true;
            }
            else
                return false;
        }
    }
}
