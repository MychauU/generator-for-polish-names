﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace projekt
{
    //
    /// <summary>
    /// klasa zawierajaca glowne informacje do oceny slowy 
    /// posiada w sobie makarow
    /// sylaby zaczynajace i konczace slowa
    /// </summary>
    /// 
    [Serializable]
    class DetailInormation
    {
        public int lowestCountOfSyllalbe;
        public int highestCountOfSyllalbe;
        public double averageCountOfSyllable;
        public int AvgCountCharacters;
        // sylaby prefix i ich ilosc wystapien
        public Dictionary<string,int> prefix;
        // sylaby postfix i ich ilosc wystapien
        public Dictionary<string,int> postfix;

        // makarow posiadajace  jako klucz poprzedni wyraz i liste sylab  z ich wystapieniami
        public Dictionary<string, Dictionary<string,int>> syllabesMakarow;
        public DetailInormation()
        {
            lowestCountOfSyllalbe = 99;
            highestCountOfSyllalbe = 0;
            AvgCountCharacters = 0;
            averageCountOfSyllable = 0;
            prefix = new Dictionary<string,int>();
            postfix = new Dictionary<string,int>();
            syllabesMakarow = new Dictionary<string, Dictionary<string, int>>();
        }


        public void Serialize(string path)
        {
            Stream stream = File.Open(path, FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(stream, this);
            stream.Close();
        }

        public static DetailInormation Deserialize(string path)
        {
            Stream stream = File.Open(path, FileMode.Open);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            return (DetailInormation)binaryFormatter.Deserialize(stream);
        }

        public static bool dataExistsAndCurrent(string path)
        {
            if (File.Exists(path))
            {
                DateTime creation = File.GetCreationTime(path);
                DateTime now = DateTime.Now;
                if ((now - creation).TotalDays>10)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else return false;
        }
    }
}
