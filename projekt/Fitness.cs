﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NHunspell;
using System.Text.RegularExpressions;

namespace projekt
{
    /// <summary>
    /// glowna klasa tworzaca imie przechowujaca informacje na temat poslkich imion itp
    /// DetailInormation detailInfM; przechowuje informacje na temat meskich imion
    /// DetailInormation detailInfZ; przechowuje informacje na temat zenskich imion
    /// List<string> meskie; lista przechowujaca meskie imiona
    /// List<string> zenskie; lista przechowujaca zenskie imiona
    /// </summary>
    class Fitness
    {
        string pathToNames;
        string pathToSyllabus;
        SpellEngine engine;
        DetailInormation detailInfM;
        DetailInormation detailInfZ;
        List<string> meskie;
        List<string> zenskie;
        public Fitness(SpellEngine engine, string pathToNames, string pathToSyllabus)
        {
            this.engine = engine;
            this.pathToNames = pathToNames;
            this.pathToSyllabus = pathToSyllabus;
            detailInfM =null;
            detailInfZ = null;
            learnToGrade();


        }

        /// <summary>
        /// glowna funkcja uczaca sie  prefix, postfix , markowa , jezeli wczesniej nie zostala nauczona wczesniej
        /// </summary>
        private void learnToGrade()
        {


            //wczytuje meskie  i zenskie do algorytmu genetycznego (zeby go nie otwierac ponownie)
            var reader = new StreamReader(File.OpenRead(pathToNames));
            meskie = new List<string>();
            zenskie = new List<string>();
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(';');
                if (values[1] == "M")
                {
                    meskie.Add(values[0]);
                }
                else
                {
                    zenskie.Add(values[0]);
                }
            }

            //deserializacja jezeli istnieje i jego data jest nowa 
            if (DetailInormation.dataExistsAndCurrent("detail_information_m.dat"))
            {
                detailInfM = DetailInormation.Deserialize("detail_information_m.dat");
                detailInfZ = DetailInormation.Deserialize("detail_information_z.dat");
                return;
            }

            detailInfM = new DetailInormation();
            detailInfZ = new DetailInormation();

            HyphenResult hyphenated;
            string[] result;
            int intpom;
            int sum = 0;
            long sumOfCharacters = 0;
            char[] charSeparators = new char[] { '=' };
            foreach (string pom in meskie)
            {
                sumOfCharacters += pom.Length;
                hyphenated = engine["pl"].Hyphenate(pom);
                result = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
                if (result != null)
                {
                    

                    int g = result.Length;
                    sum = sum + g;
                    if (g < detailInfM.lowestCountOfSyllalbe)
                        detailInfM.lowestCountOfSyllalbe = g;
                    else if (g > detailInfM.highestCountOfSyllalbe)
                        detailInfM.highestCountOfSyllalbe = g;


                    Dictionary<string, int> pomKeyValue;
                    int pomDicInt;
                    //ladowanie makarow (czyli tablicy powiazan miedzy sylabami)
                    for (int i = 0; i < g-1;i++)
                    {
                        if (detailInfM.syllabesMakarow.TryGetValue(result[i], out pomKeyValue))
                        {
                            if (pomKeyValue.TryGetValue(result[i+1], out pomDicInt))
                            {
                                pomKeyValue[result[i + 1]]++;
                            }
                            else
                            {
                                pomKeyValue.Add(result[i + 1], 1);
                            }
                        }
                        else
                        {
                            detailInfM.syllabesMakarow.Add(result[i], new Dictionary<string, int>());
                        }

                    }

                    //dodanie count dla prefix jezeli istnieje lub dodanie nowego dictionary
                    //    var response = detailInfM.prefix.Find(r => r.syllable == result[0]);
                    if (detailInfM.prefix.TryGetValue(result[0], out intpom))
                    {
                        intpom++;
                    }
                    else {
                        detailInfM.prefix.Add(result[0],1);
                    }

                    //  response = detailInfM.postfix.Find(r => r.syllable == result[g - 1]);

                    if (detailInfM.postfix.TryGetValue(result[g - 1], out intpom))
                    {
                        intpom++;
                    }
                    else {
                        detailInfM.postfix.Add(result[g-1], 1);
                        //    detailInfM.postfix.Add(new Syllabus(result[g - 1]));
                    }
                }
            }
            detailInfM.averageCountOfSyllable = (double)sum / meskie.Count;
            detailInfM.AvgCountCharacters =( int)(sumOfCharacters / meskie.Count);
            sumOfCharacters = 0;
            sum = 0;



            foreach (string pom in zenskie)
            {
                sumOfCharacters += pom.Length;
                hyphenated = engine["pl"].Hyphenate(pom);
                result = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
                if (result != null)
                {
                    int g = result.Length;
                    sum = sum + g;
                    if (g < detailInfZ.lowestCountOfSyllalbe)
                        detailInfZ.lowestCountOfSyllalbe = g;
                    else if (g > detailInfZ.highestCountOfSyllalbe)
                        detailInfZ.highestCountOfSyllalbe = g;


                    Dictionary<string, int> pomKeyValue;
                    int pomDicInt;
                    //ladowanie makarow
                    for (int i = 0; i < g - 1; i++)
                    {
                        if (detailInfZ.syllabesMakarow.TryGetValue(result[i], out pomKeyValue))
                        {
                            if (pomKeyValue.TryGetValue(result[i + 1], out pomDicInt))
                            {
                                pomKeyValue[result[i + 1]]++;
                            }
                            else
                            {
                                pomKeyValue.Add(result[i + 1], 1);
                            }
                        }
                        else
                        {
                            detailInfZ.syllabesMakarow.Add(result[i], new Dictionary<string, int>());
                        }

                    }

                    // var response = detailInfZ.prefix.Find(r => r.syllable == result[0]);
                    if (detailInfZ.prefix.TryGetValue(result[0], out intpom))
                    {
                        intpom++;
                    }
                    else {
                        detailInfZ.prefix.Add(result[0], 1);
                    }

                    //  response = detailInfM.postfix.Find(r => r.syllable == result[g - 1]);

                    if (detailInfZ.postfix.TryGetValue(result[g - 1], out intpom))
                    {
                        intpom++;
                    }
                    else {
                        detailInfZ.postfix.Add(result[g - 1], 1);
                        //    detailInfM.postfix.Add(new Syllabus(result[g - 1]));
                    }
                }
            }
            detailInfZ.averageCountOfSyllable = (double)sum / zenskie.Count;
            detailInfZ.AvgCountCharacters =( int)(sumOfCharacters / zenskie.Count);

            detailInfM.Serialize("detail_information_m.dat");
            detailInfZ.Serialize("detail_information_z.dat");

        }

        

        
        /// <summary>
        /// funkcja tworzaca meskie imie 
        /// </summary>
        /// <returns></returns>
        public string createManName()
        {
            string newName = "";
            double probability = 0;
            //get 100 random man names
            WordGrade[] menNames = new WordGrade[100];
            int rand = 0;
            int count = 0;
            string line;
            string[] result;
            char[] charSeparators = new char[] { ';' };
            Random random = new Random();
            string bestName;
            int numberOfAllMenNames = meskie.Count;
            while (count < 100)
            {
                rand = random.Next(numberOfAllMenNames);
                menNames[count] = new WordGrade();
                menNames[count++].name = meskie[rand];
            }

            //prawo mutacji  ,krzyzowania
            double crossOverRule = 0.3;
            double mutationRule = 0.1;
            //ilosc epok
            int milennia = 300;
            //tablica nowych imion (uzyskanych z turniejow)
            WordGrade[] newMenNames = new WordGrade[100];

            for (int i = 0; i < milennia; i++)
            {
                //tutaj ocena tzw populacji czyli tych 100 imion (czyli funkcja fitness)

                /*UWAGA TRZEBA NAPEWNO POLEPSZYC*/
                fitnessGradeOfMenNames(ref menNames);
                Array.Sort(menNames);

                //zapisanie najlepszego osobnikow np 1 (elitaryzm)
                bestName = menNames[0].name;

                //selekcja turniejowa
                for (int j = 0; j < menNames.Length; j++)
                {
                    int obj1 = random.Next(menNames.Length);
                    int obj2 = random.Next(menNames.Length);
                    if (menNames[obj1].grade >= menNames[obj2].grade)
                    {
                        newMenNames[j] = menNames[obj1];
                    }
                    else
                    {
                        newMenNames[j] = menNames[obj2];
                    }
                }

                //crossover (krzyzowanie)
                for (int j = 0; j < menNames.Length / 2; j++)
                {
                    /*UWAGA tutaj moglobybyc  zamiast wybierania  kazdego z kazdym od najlepszych do najgorszych lepiej byloby miec
                    randoma opartego na dystrybuancie np 1/x^2
                    */
                    probability = random.NextDouble();
                    if (probability <= crossOverRule)
                    {
                        probability = random.NextDouble();
                        //https://en.wikipedia.org/wiki/Crossover_(genetic_algorithm)
                        if (probability < 0.1 && probability >= 0.0)
                        {
                            onePointCrossover(ref newMenNames[2 * j].name, ref newMenNames[2 * j + 1].name);
                        }
                        else if (probability < 0.2 && probability >= 0.1)
                        {
                            twoPointCrossover(ref newMenNames[2 * j].name, ref newMenNames[2 * j + 1].name);
                        }
                        else if (probability < 0.3 && probability >= 0.2)
                        {
                            cutAndSpliceCrossover(ref newMenNames[2 * j].name, ref newMenNames[2 * j + 1].name);
                        }
                        else if (probability < 0.4 && probability >= 0.3)
                        {
                            uniformCrossover(ref newMenNames[2 * j].name, ref newMenNames[2 * j + 1].name);
                        }
                    }

                }
                //mutacje
                for (int j = 0; j < menNames.Length; j++)
                {
                    probability = random.NextDouble();
                    if (probability <= mutationRule)
                    {
                        probability = random.NextDouble();
                        if (probability < 0.1 && probability >= 0.0)
                        {
                            addSyllableMutation(ref newMenNames[j].name);
                        }
                        else if (probability < 0.2 && probability >= 0.1)
                        {
                            deleteSyllableMutation(ref newMenNames[j].name);
                        }
                        else if (probability < 0.3 && probability >= 0.2)
                        {
                            inversionSyllableMutation(ref newMenNames[j].name);
                        }
                        else if (probability < 0.4 && probability >= 0.3)
                        {
                            twoPointShiftSyllableMutation(ref newMenNames[j].name);
                        }

                    }
                }

                //zastapienie starej populacji nowa
                menNames = newMenNames;
                //elitaryzm
                menNames[menNames.Length - 1].name = bestName;




            }
            fitnessGradeOfMenNames(ref menNames);
            Array.Sort(menNames);
            newName = menNames[random.Next(6)].name;
            newName = newName.ToLower();
            char c = Char.ToUpper(newName[0]);
            newName = newName.Remove(0, 1);
            newName = c + newName;
            return newName;

        }

        /// <summary>
        /// 
        /// funkcja tworzaca zenskie imie
        /// </summary>
        /// <returns></returns>
        public string createWomanName()
        {
            string newName = "";
            double probability = 0;
            //get 100 random man names
            WordGrade[] womenNames = new WordGrade[100];
            int rand = 0;
            int count = 0;
            string line;
            string[] result;
            char[] charSeparators = new char[] { ';' };
            Random random = new Random();
            string bestName;
            //var lineCountOfFile = File.ReadLines(pathToNames).Count();
            int numberOfAllWomenNames = zenskie.Count;
            while (count < 100)
            {
                rand = random.Next(numberOfAllWomenNames);
                womenNames[count] = new WordGrade();
                womenNames[count++].name = zenskie[rand];
            }

            //prawo mutacji  ,krzyzowania
            double crossOverRule = 0.3;
            double mutationRule = 0.1;
            //ilosc epok
            int milennia = 300;
            //tablica nowych imion (uzyskanych z turniejow)
            WordGrade[] newWomenNames = new WordGrade[100];

            for (int i = 0; i < milennia; i++)
            {
                //tutaj ocena tzw populacji czyli tych 100 imion (czyli funkcja fitness)

                /*UWAGA TRZEBA NAPEWNO POLEPSZYC*/
                fitnessGradeOfWomenNames(ref womenNames);
                Array.Sort(womenNames);

                //zapisanie najlepszego osobnikow np 1 (elitaryzm)
                bestName = womenNames[0].name;

                //selekcja turniejowa
                for (int j = 0; j < womenNames.Length; j++)
                {
                    int obj1 = random.Next(womenNames.Length);
                    int obj2 = random.Next(womenNames.Length);
                    if (womenNames[obj1].grade >= womenNames[obj2].grade)
                    {
                        newWomenNames[j] = womenNames[obj1];
                    }
                    else
                    {
                        newWomenNames[j] = womenNames[obj2];
                    }
                }

                //crossover (krzyzowanie)
                for (int j = 0; j < womenNames.Length / 2; j++)
                {
                    /*UWAGA tutaj moglobybyc  zamiast wybierania  kazdego z kazdym od najlepszych do najgorszych lepiej byloby miec
                    randoma opartego na dystrybuancie np 1/x^2
                    */
                    probability = random.NextDouble();
                    if (probability <= crossOverRule)
                    {
                        probability = random.NextDouble();
                        //https://en.wikipedia.org/wiki/Crossover_(genetic_algorithm)
                        if (probability < 0.1 && probability >= 0.0)
                        {
                            onePointCrossover(ref newWomenNames[2 * j].name, ref newWomenNames[2 * j + 1].name);
                        }
                        else if (probability < 0.2 && probability >= 0.1)
                        {
                            twoPointCrossover(ref newWomenNames[2 * j].name, ref newWomenNames[2 * j + 1].name);
                        }
                        else if (probability < 0.3 && probability >= 0.2)
                        {
                            cutAndSpliceCrossover(ref newWomenNames[2 * j].name, ref newWomenNames[2 * j + 1].name);
                        }
                        else if (probability < 0.4 && probability >= 0.3)
                        {
                            uniformCrossover(ref newWomenNames[2 * j].name, ref newWomenNames[2 * j + 1].name);
                        }
                    }

                }
                //mutacje
                for (int j = 0; j < womenNames.Length; j++)
                {
                    probability = random.NextDouble();
                    if (probability <= mutationRule)
                    {
                        probability = random.NextDouble();
                        if (probability < 0.1 && probability >= 0.0)
                        {
                            addSyllableMutation(ref newWomenNames[j].name);
                        }
                        else if (probability < 0.2 && probability >= 0.1)
                        {
                            deleteSyllableMutation(ref newWomenNames[j].name);
                        }
                        else if (probability < 0.3 && probability >= 0.2)
                        {
                            inversionSyllableMutation(ref newWomenNames[j].name);
                        }
                        else if (probability < 0.4 && probability >= 0.3)
                        {
                            twoPointShiftSyllableMutation(ref newWomenNames[j].name);
                        }

                    }
                }

                //zastapienie starej populacji nowa
                womenNames = newWomenNames;
                //elitaryzm
                womenNames[womenNames.Length - 1].name = bestName;




            }
            fitnessGradeOfWomenNames(ref womenNames);
            Array.Sort(womenNames);
            newName = womenNames[random.Next(6)].name;
            newName = newName.ToLower();
            char c = Char.ToUpper(newName[0]);
            newName = newName.Remove(0, 1);
            newName = c + newName;
            return newName;

        }

        

        private void inversionSyllableMutation(ref string name)
        {
            HyphenResult hyphenated;
            string[] result1;
            int randPoint1;
            int randPoint2;
            string pom;
            int intPom;
            Random random = new Random();
            char[] charSeparators = new char[] { '=' };
            hyphenated = engine["pl"].Hyphenate(name);
            result1 = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            randPoint1 = random.Next(result1.Length);
            randPoint2 = random.Next(result1.Length);
            if (randPoint1 > randPoint2)
            {
                intPom = randPoint1;
                randPoint1 = randPoint2;
                randPoint2 = intPom;
            }
            int j = 0;
            for (int i = randPoint1; i < randPoint2; i++)
            {
                pom = result1[i];
                result1[i] = result1[randPoint2 - j];
                result1[randPoint2 - j] = pom;
                j++;
            }

            name = String.Join(String.Empty, result1);
        }

        private void twoPointShiftSyllableMutation(ref string name)
        {
            HyphenResult hyphenated;
            string[] result1;
            int randPoint1;
            int randPoint2;
            string pom;
            Random random = new Random();
            char[] charSeparators = new char[] { '=' };
            hyphenated = engine["pl"].Hyphenate(name);
            result1 = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            randPoint1 = random.Next(result1.Length);
            randPoint2 = random.Next(result1.Length);
            pom = result1[randPoint1];
            result1[randPoint1] = result1[randPoint2];
            result1[randPoint2] = pom;

            name = String.Join(String.Empty, result1);

        }


        private void deleteSyllableMutation(ref string name)
        {
            HyphenResult hyphenated;
            string[] result1;
            string[] newWord;
            int randPoint;
            Random random = new Random();
            char[] charSeparators = new char[] { '=' };
            hyphenated = engine["pl"].Hyphenate(name);
            result1 = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            if (result1.Length <= 1)
            {
                return;
            }
            else {
                randPoint = random.Next(result1.Length);
                newWord = new string[result1.Length - 1];
                for (int i = 0; i < randPoint; i++)
                {
                    newWord[i] = result1[i];

                }
                for (int i = randPoint; i < newWord.Length; i++)
                {
                    newWord[i] = result1[i + 1];

                }
                name = String.Join(String.Empty, newWord);
            }
        }

        private void addSyllableMutation(ref string name)
        {
            HyphenResult hyphenated;
            string[] result1;
            string[] result2;
            string[] newWord;
            string[] pomWord;
            string word;
            int randPoint;
            Random random = new Random();
            char[] charSeparators = new char[] { '=', ';' };

            var lineCountOfFile = File.ReadLines(pathToNames).Count();
            randPoint = random.Next(2);
            if (randPoint == 0)
            {
                randPoint = random.Next(meskie.Count);
                word = meskie[randPoint];
            }
            else
            {
                randPoint = random.Next(zenskie.Count);
                word = zenskie[randPoint];
            }
            pomWord = word.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);

            hyphenated = engine["pl"].Hyphenate(pomWord[0]);
            result2 = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);

            hyphenated = engine["pl"].Hyphenate(name);
            result1 = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            randPoint = random.Next(result1.Length);
            newWord = new string[result1.Length + 1];
            for (int i = 0; i < randPoint; i++)
            {
                newWord[i] = result1[i];

            }
            newWord[randPoint] = result2[random.Next(result2.Length)];

            for (int i = randPoint + 1; i < newWord.Length; i++)
            {
                newWord[i] = result1[i - 1];

            }
            name = String.Join(String.Empty, newWord);

        }

        private void uniformCrossover(ref string v1, ref string v2)
        {
            HyphenResult hyphenated;
            string[] result1;
            string[] result2;
            char[] charSeparators = new char[] { '=' };
            string pom;
            double probabilty = 0;
            hyphenated = engine["pl"].Hyphenate(v1);
            result1 = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            hyphenated = engine["pl"].Hyphenate(v2);
            result2 = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            Random random = new Random();
            if (result1.Length >= result2.Length)
            {

                for (int i = 0; i < result2.Length; i++)
                {
                    probabilty = random.NextDouble();
                    if (probabilty < 0.5)
                    {
                        pom = result1[i];
                        result1[i] = result2[i];
                        result2[i] = pom;
                    }

                }
            }
            else
            {
                for (int i = 0; i < result1.Length; i++)
                {
                    probabilty = random.NextDouble();
                    if (probabilty < 0.5)
                    {
                        pom = result1[i];
                        result1[i] = result2[i];
                        result2[i] = pom;
                    }

                }
            }
            v1 = String.Join(String.Empty, result1);
            v2 = String.Join(String.Empty, result2);
        }

        private void cutAndSpliceCrossover(ref string v1, ref string v2)
        {
            HyphenResult hyphenated;
            string[] result1;
            string[] result2;
            char[] charSeparators = new char[] { '=' };
            string pom;
            hyphenated = engine["pl"].Hyphenate(v1);
            result1 = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            hyphenated = engine["pl"].Hyphenate(v2);
            result2 = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            Random random = new Random();
            int randPoint1 = 0;
            int randPoint2 = 0;
            int intPom = 0;
            int j = 0;
            randPoint1 = random.Next(result1.Length);
            randPoint2 = random.Next(result2.Length);

            string[] v1New = new string[randPoint1 + result2.Length - randPoint2];
            string[] v2New = new string[randPoint2 + result1.Length - randPoint1];

            for (int i = 0; i < randPoint1; i++)
            {
                v1New[i] = result1[i];

            }
            for (int i = 0; i < randPoint2; i++)
            {
                v2New[i] = result2[i];

            }
            for (int i = randPoint1; i < v1New.Length; i++)
            {
                v1New[i] = result2[randPoint2 + j];
                j++;

            }
            j = 0;
            for (int i = randPoint2; i < v2New.Length; i++)
            {
                v2New[i] = result1[randPoint1 + j];
                j++;

            }


            v1 = String.Join(String.Empty, v1New);
            v2 = String.Join(String.Empty, v2New);


        }

        private void twoPointCrossover(ref string v1, ref string v2)
        {
            HyphenResult hyphenated;
            string[] result1;
            string[] result2;
            char[] charSeparators = new char[] { '=' };
            string pom;
            hyphenated = engine["pl"].Hyphenate(v1);
            result1 = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            hyphenated = engine["pl"].Hyphenate(v2);
            result2 = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            Random random = new Random();
            int randPoint1 = 0;
            int randPoint2 = 0;
            int intPom = 0;
            if (result1.Length >= result2.Length)
            {
                randPoint1 = random.Next(result2.Length);
                randPoint2 = random.Next(result2.Length);
                if (randPoint1 > randPoint2)
                {
                    intPom = randPoint1;
                    randPoint1 = randPoint2;
                    randPoint2 = intPom;
                }
                for (int i = randPoint1; i < randPoint2; i++)
                {
                    pom = result1[i];
                    result1[i] = result2[i];
                    result2[i] = pom;

                }
            }
            else
            {
                randPoint1 = random.Next(result1.Length);
                randPoint2 = random.Next(result1.Length);
                if (randPoint1 > randPoint2)
                {
                    intPom = randPoint1;
                    randPoint1 = randPoint2;
                    randPoint2 = intPom;
                }
                for (int i = randPoint1; i < randPoint2; i++)
                {
                    pom = result1[i];
                    result1[i] = result2[i];
                    result2[i] = pom;

                }
            }
            v1 = String.Join(String.Empty, result1);
            v2 = String.Join(String.Empty, result2);

        }

        private void onePointCrossover(ref string v1, ref string v2)
        {
            HyphenResult hyphenated;
            string[] result1;
            string[] result2;
            char[] charSeparators = new char[] { '=' };
            string pom;
            hyphenated = engine["pl"].Hyphenate(v1);
            result1 = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            hyphenated = engine["pl"].Hyphenate(v2);
            result2 = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            Random random = new Random();
            int randPoint = 0;
            if (result1.Length >= result2.Length)
            {
                randPoint = random.Next(result2.Length);
                for (int i = randPoint; i < result2.Length; i++)
                {
                    pom = result1[i];
                    result1[i] = result2[i];
                    result2[i] = pom;

                }
            }
            else
            {
                randPoint = random.Next(result1.Length);
                for (int i = randPoint; i < result1.Length; i++)
                {
                    pom = result1[i];
                    result1[i] = result2[i];
                    result2[i] = pom;

                }
            }
            v1 = String.Join(String.Empty, result1);
            v2 = String.Join(String.Empty, result2);


        }

        private void fitnessGradeOfMenNames(ref WordGrade[] gradeOfMenNames)
        {
            HyphenResult hyphenated;
            string[] result;
            char[] charSeparators = new char[] { '=' };
            for (int i = 0; i < gradeOfMenNames.Length; i++)
            {
                hyphenated = engine["pl"].Hyphenate(gradeOfMenNames[i].name);
                gradeOfMenNames[i].grade = 0;
                result = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);

                //ocena ilosci sylab i znakow
                gradeOfMenNames[i].grade = gradeOfMenNames[i].grade + checkMenAnatomyOfWord(gradeOfMenNames[i].name, result.Length);

                //ocena jako sylaby: unikalnosc, powtorzenia, czy palindrom
                gradeOfMenNames[i].grade = gradeOfMenNames[i].grade + checkUniqueMenFeatures(gradeOfMenNames[i].name, result);

                //sprawdzanie prefix 
                gradeOfMenNames[i].grade = gradeOfMenNames[i].grade + checkMenPrefix(result[0]);
                //sprawdzanie postfix
                gradeOfMenNames[i].grade = gradeOfMenNames[i].grade + checkMenPostfix(result[result.Length - 1]);

                //sprawdzanie czy istnieje w slowniku
                gradeOfMenNames[i].grade = gradeOfMenNames[i].grade + checkMenNameIfExistAlready(gradeOfMenNames[i].name);
            }


        }

        private void fitnessGradeOfWomenNames(ref WordGrade[] womenNames)
        {
            HyphenResult hyphenated;
            string[] result;
            char[] charSeparators = new char[] { '=' };
            for (int i = 0; i < womenNames.Length; i++)
            {
                hyphenated = engine["pl"].Hyphenate(womenNames[i].name);
                womenNames[i].grade = 0;
                result = hyphenated.HyphenatedWord.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);

                //ocena ilosci sylab i znakow
                womenNames[i].grade = womenNames[i].grade + checkWomenAnatomyOfWord(womenNames[i].name, result.Length);

                //ocena jako sylaby: unikalnosc, powtorzenia, czy palindrom
                womenNames[i].grade = womenNames[i].grade + checkUniqueWomenFeatures(womenNames[i].name, result);

                //sprawdzanie prefix 
                womenNames[i].grade = womenNames[i].grade + checkWomenPrefix(result[0]);
                //sprawdzanie postfix
                womenNames[i].grade = womenNames[i].grade + checkWomenPostfix(result[result.Length - 1]);
                //sprawdzanie czy istnieje w slowniku
                womenNames[i].grade = womenNames[i].grade + checkWomenNameIfExistAlready(womenNames[i].name);

            }
        }

        private double checkMenNameIfExistAlready(string name)
        {
            double ocena = 0;
            var match = meskie.FirstOrDefault(stringToCheck => stringToCheck.Contains(name));

            if (match != null)
            {
                ocena -= 10;
            }
            return ocena;
        }

        private double checkWomenNameIfExistAlready(string name)
        {
            double ocena = 0;
            var match = zenskie.FirstOrDefault(stringToCheck => stringToCheck.Contains(name));

            if (match != null)
            {
                ocena -= 10;
            }
            return ocena;
        }

        private double checkUniqueMenFeatures(string word, string[] result)
        {
            double ocena = 0;
            bool isPalindrom = false;
            //czy sylaby dobrze sie rozkladaja (tzn czy nie ma sylaby bez zadnej samogloski)
            Regex regex = new Regex(@"^[A-Za-zśćżźęąółń]*((i|y|e|a|o|u|ą|ę|ł|ń|ś|ż|ź|ć|ó)+)[A-Za-zśćżźęąółń]*$");
            foreach (string part in result)
            {
                if (!regex.IsMatch(part))
                {
                    ocena -= 15;
                    return ocena;
                }
            }

            //czy sylaby sie powtarzaja
            // oraz ocena Makarow
            Dictionary<string, int> pomKeyValue;
            int pomDicInt;
            for (int i = 0; i < result.Length - 1; i++)
            {
                if (detailInfM.syllabesMakarow.TryGetValue(result[i], out pomKeyValue))
                {
                    if (pomKeyValue.TryGetValue(result[i + 1], out pomDicInt))
                    {
                        ocena += pomDicInt;
                    }
                }
                for (int j = i + 1; j < result.Length; j++)
                {
                    if (result[i] == result[j])
                    {
                        ocena -= 4;
                    }
                }
            }


            //czy palindrom
            isPalindrom = true;
            for (int i = 0; i < word.Length / 2; i++)
            {
                if (word[i] != word[word.Length - 1 - i])
                {
                    isPalindrom = false;
                    break;
                }

            }
            if (isPalindrom)
            {
                ocena += 8;
            }
            return ocena;
        }


        private double checkMenAnatomyOfWord(string word, int length)
        {
            double ocena = 0;
            //ocena 4 gdy ilosc znakow nie przekracza normy else 2
            if (word.Length <= this.detailInfM.AvgCountCharacters - 6 || word.Length >= this.detailInfM.AvgCountCharacters + 6)
                ocena -= Math.Abs(word.Length - this.detailInfM.AvgCountCharacters);
            else
            {
                ocena += Math.Abs(word.Length - this.detailInfM.AvgCountCharacters);
            }
            //ilosc sylab
            if (length <= this.detailInfM.averageCountOfSyllable - 2 || word.Length >= this.detailInfM.averageCountOfSyllable + 2)
                ocena -= Math.Abs(length - this.detailInfM.averageCountOfSyllable);
            else
            {
                ocena += Math.Abs(length - this.detailInfM.averageCountOfSyllable);
            }

            //czy posiada polskie znaki
            Regex regex = new Regex(@"^(ą|ę|ł|ń|ś|ż|ź|ć|ó|sz|cz|rz)?$");
            if (regex.IsMatch(word))
            {
                ocena += 1;
            }
            return ocena;

        }

        private double checkWomenAnatomyOfWord(string word, int length)
        {
            double ocena = 0;
            if (word.Length <= this.detailInfZ.AvgCountCharacters - 6 || word.Length >= this.detailInfZ.AvgCountCharacters + 6)
                ocena -= Math.Abs(word.Length - this.detailInfZ.AvgCountCharacters);
            else
            {
                ocena += Math.Abs(word.Length - this.detailInfZ.AvgCountCharacters);
            }
            //ilosc sylab
            if (length <= this.detailInfZ.averageCountOfSyllable - 2 || word.Length >= this.detailInfZ.averageCountOfSyllable + 2)
                ocena -= Math.Abs(length - this.detailInfZ.averageCountOfSyllable);
            else
            {
                ocena += Math.Abs(length - this.detailInfZ.averageCountOfSyllable);
            }

            //czy posiada polskie znaki
            Regex regex = new Regex(@"^(ą|ę|ł|ń|ś|ż|ź|ć|ó|sz|cz|rz)?$");
            if (regex.IsMatch(word))
            {
                ocena += 1;
            }
            return ocena;

        }

        private double checkUniqueWomenFeatures(string word, string[] result)
        {
            double ocena = 0;
            bool isPalindrom = false;
            //czy sylaby dobrze sie rozkladaja (tzn czy nie ma sylaby bez zadnej samogloski)
            Regex regex = new Regex(@"^[A-Za-zśćżźęąółń]*((i|y|e|a|o|u|ą|ę|ł|ń|ś|ż|ź|ć|ó)+)[A-Za-zśćżźęąółń]*$");
            foreach (string part in result)
            {
                if (!regex.IsMatch(part))
                {
                    ocena -= 15;
                    return ocena;
                }
            }

            //czy sylaby sie powtarzaja
            // oraz ocena Makarow
            Dictionary<string, int> pomKeyValue;
            int pomDicInt;
            for (int i = 0; i < result.Length - 1; i++)
            {
                if (detailInfZ.syllabesMakarow.TryGetValue(result[i], out pomKeyValue))
                {
                    if (pomKeyValue.TryGetValue(result[i + 1], out pomDicInt))
                    {
                        ocena += pomDicInt;
                    }
                }
                for (int j = i + 1; j < result.Length; j++)
                {
                    if (result[i] == result[j])
                    {
                        ocena -= 4;
                    }
                }
            }


            //czy palindrom
            isPalindrom = true;
            for (int i = 0; i < word.Length / 2; i++)
            {
                if (word[i] != word[word.Length - 1 - i])
                {
                    isPalindrom = false;
                    break;
                }

            }
            if (isPalindrom)
            {
                ocena += 8;
            }
            return ocena;
        }

        private double checkMenPrefix(string part)
        {
            double ocena = 0;

            //jezeli posiada prefix znany juz ocena +5
            //nieznany +3
            //jezeli prefix nie jest sylabą po zlaczeniu ocena 0
            int pom;
            if (detailInfM.prefix.TryGetValue(part, out pom))
            {
                ocena = ocena + pom + 5;
            }
            else
            {
                ocena += 3;
            }
            return ocena;
        }


        private double checkMenPostfix(string part)
        {
            double ocena = 0;

            //jezeli posiada prefix znany juz ocena +5
            //nieznany +3
            //jezeli prefix nie jest sylabą po zlaczeniu ocena 0
            //jezeli konczy się na ć przewidywany czasownik ocena 0
            if (part[part.Length - 1] == 'ć' || part[part.Length - 1] == 'y')
            {
                ocena -= 5;
            }
            int pom;
            if (detailInfM.postfix.TryGetValue(part, out pom))
            {
                ocena = ocena + pom + 5;
            }
            else
            {
                ocena += 3;
            }
            return ocena;
        }

        private double checkWomenPrefix(string part)
        {
            double ocena = 0;

            //jezeli posiada prefix znany juz ocena +5
            //nieznany +3
            //jezeli prefix nie jest sylabą po zlaczeniu ocena 0
            int pom;   
            if (detailInfZ.prefix.TryGetValue(part, out pom))
            {
                ocena =ocena+pom+5;
            }
            else
            {
                ocena += 3;
            }
            return ocena;
        }


        private double checkWomenPostfix(string part)
        {
            double ocena = 0;

            //jezeli posiada prefix znany juz ocena +5
            //nieznany +3
            //jezeli prefix nie jest sylabą po zlaczeniu ocena 0
            //jezeli konczy się na ć przewidywany czasownik ocena 0
            if (part[part.Length - 1] == 'ć' || part[part.Length - 1] == 'y')
            {
                ocena -= 5;
            }
            else if (part[part.Length - 1] == 'a' || part[part.Length - 1] == 'e')
            {
                ocena += 5;
            }
            int pom;
            if (detailInfZ.postfix.TryGetValue(part, out pom))
            {
                ocena = ocena + pom + 5;
            }
            else
            {
                ocena += 3;
            }
            return ocena;
        }
    }
}
